#!/bin/bash

# $1 param: cantidadTests
# Run all tests

# Start number (could be 0 or 1)
n=1

# Number of tests
cantidadTests=$1

while [ $n -le $cantidadTests ]
do
	echo "Test [$n/$cantidadTests]"
	echo "OK"
	n=$(( n+1 ))	 
done